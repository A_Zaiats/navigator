package io.github.azaiats.navigator.command;

import android.support.annotation.Nullable;

import io.github.azaiats.navigator.screen.Screen;

/**
 * @author Andrei Zaiats
 * @since 09/30/2017
 */
public class BackTo implements Command {

    private final Class<Screen> screenClass;

    public BackTo(@Nullable Class<Screen> screenClass) {
        this.screenClass = screenClass;
    }

    @Nullable
    public String getScreenName() {
        return screenClass == null ? null : screenClass.getName();
    }
}
