package io.github.azaiats.navigator.android;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import io.github.azaiats.navigator.Navigator;
import io.github.azaiats.navigator.command.Back;
import io.github.azaiats.navigator.command.BackTo;
import io.github.azaiats.navigator.command.Command;
import io.github.azaiats.navigator.command.Forward;
import io.github.azaiats.navigator.command.Replace;
import io.github.azaiats.navigator.command.SystemMessage;
import io.github.azaiats.navigator.screen.FragmentScreen;
import io.github.azaiats.navigator.screen.Screen;

/**
 * @author Andrei Zaiats
 * @since 10/02/2017
 */
public abstract class FragmentNavigator implements Navigator {

    private final FragmentManager fragmentManager;
    private final int containerId;

    public FragmentNavigator(FragmentManager fragmentManager, int containerId) {
        this.fragmentManager = fragmentManager;
        this.containerId = containerId;
    }

    @Override
    public void applyCommand(@NonNull Command command) {
        if (command instanceof Forward) {
            forward((Forward) command);
        } else if (command instanceof Replace) {
            replace((Replace) command);
        } else if (command instanceof Back) {
            back();
        } else if (command instanceof BackTo) {
            backTo((BackTo) command);
        } else if (command instanceof SystemMessage) {
            showSystemMessage(((SystemMessage) command).getMessage());
        }
    }

    protected abstract void showSystemMessage(@NonNull String message);
    protected abstract void exit();

    /**
     * Override this method to setup custom fragment transaction animation.
     *
     * @param command             current navigation command. Will be only {@link Forward} or {@link Replace}
     * @param currentFragment     current fragment in container
     *                            (for {@link Replace} command it will be screen previous in new chain, NOT replaced screen)
     * @param nextFragment        next screen fragment
     * @param fragmentTransaction fragment transaction
     */
    protected void setupFragmentTransactionAnimation(@NonNull Command command, @Nullable Fragment currentFragment,
                                                     @NonNull Fragment nextFragment, @NonNull FragmentTransaction fragmentTransaction) {

    }

    protected void backToUnexisting() {
        backToRoot();
    }

    private void forward(Forward command) {
        final Screen screen = command.getScreen();
        if (screen instanceof FragmentScreen) {
            forward(command, (FragmentScreen) screen);
        }
    }

    private void forward(Forward command, FragmentScreen screen) {
        final Fragment fragment = screen.getFragment();
        final FragmentTransaction transaction = fragmentManager.beginTransaction();
        setupFragmentTransactionAnimation(command, fragmentManager.findFragmentById(containerId), fragment, transaction);
        transaction.replace(containerId, fragment).addToBackStack(screen.getClass().getName()).commit();
    }

    private void replace(Replace command) {
        final Screen screen = command.getScreen();
        if (screen instanceof FragmentScreen) {
            replace(command, (FragmentScreen) screen);
        }
    }

    private void replace(Replace command, FragmentScreen screen) {
        final Fragment fragment = screen.getFragment();

        if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStack();

            final FragmentTransaction transaction = fragmentManager.beginTransaction();
            setupFragmentTransactionAnimation(command, fragmentManager.findFragmentById(containerId), fragment, transaction);
            transaction.replace(containerId, fragment).addToBackStack(screen.getClass().getName()).commit();
        } else {
            final FragmentTransaction transaction = fragmentManager.beginTransaction();
            setupFragmentTransactionAnimation(command, fragmentManager.findFragmentById(containerId), fragment, transaction);
            transaction.replace(containerId, fragment).commit();
        }
    }

    private void backTo(BackTo command) {
        final String screenName = command.getScreenName();
        if (screenName == null) {
            backToRoot();
        } else {
            backTo(screenName);
        }
    }

    private void back() {
        if (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStackImmediate();
        } else {
            exit();
        }
    }

    private void backTo(String screenKey) {
        boolean hasScreen = false;
        for (int i = 0; i < fragmentManager.getBackStackEntryCount(); i++) {
            if (screenKey.equals(fragmentManager.getBackStackEntryAt(i).getName())) {
                fragmentManager.popBackStack(screenKey, 0);
                hasScreen = true;
                break;
            }
        }

        if (!hasScreen) {
            backToUnexisting();
        }
    }

    private void backToRoot() {
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }
}
