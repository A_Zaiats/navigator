package io.github.azaiats.navigator;

import android.support.annotation.NonNull;

import io.github.azaiats.navigator.command.Command;

/**
 * @author Andrei Zaiats
 * @since 09/30/2017
 */
public interface Navigator {
    void applyCommand(@NonNull Command command);
}
