package io.github.azaiats.navigator.screen;

import android.app.Fragment;
import android.support.annotation.NonNull;

/**
 * @author Andrei Zaiats
 * @since 09/30/2017
 */
public abstract class FragmentScreen implements Screen {

    @NonNull
    public abstract Fragment getFragment();
}
