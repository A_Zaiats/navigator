package io.github.azaiats.navigator;

import android.support.annotation.NonNull;

/**
 * @author Andrei Zaiats
 * @since 09/30/2017
 */
public interface NavigatorHolder {
    void setNavigator(@NonNull Navigator navigator);
    void removeNavigator();
}
