package io.github.azaiats.navigator.android;

import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;

import io.github.azaiats.navigator.command.Command;
import io.github.azaiats.navigator.command.Forward;
import io.github.azaiats.navigator.command.Replace;
import io.github.azaiats.navigator.screen.ActivityScreen;
import io.github.azaiats.navigator.screen.Screen;

/**
 * @author Andrei Zaiats
 * @since 09/30/2017
 */
public abstract class SupportAppNavigator extends SupportFragmentNavigator {

    private final FragmentActivity activity;

    public SupportAppNavigator(@NonNull FragmentActivity activity, @IdRes int containerId) {
        super(activity.getSupportFragmentManager(), containerId);
        this.activity = activity;
    }

    @Override
    public void applyCommand(@NonNull Command command) {
        if (command instanceof Forward) {
            forward((Forward) command);
            return;
        } else if (command instanceof Replace) {
            replace((Replace) command);
            return;
        }
        super.applyCommand(command);
    }

    protected void exit() {
        activity.finish();
    }

    private void forward(Forward command) {
        final Screen screen = command.getScreen();
        if (screen instanceof ActivityScreen) {
            forward((ActivityScreen) screen);
        } else {
            super.applyCommand(command);
        }
    }

    private void forward(ActivityScreen screen) {
        activity.startActivity(screen.getIntent(activity));
    }

    private void replace(Replace command) {
        final Screen screen = command.getScreen();
        if (screen instanceof ActivityScreen) {
            replace((ActivityScreen) screen);
        } else {
            super.applyCommand(command);
        }
    }

    private void replace(ActivityScreen screen) {
        activity.startActivity(screen.getIntent(activity));
        activity.finish();
    }
}
