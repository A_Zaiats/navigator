package io.github.azaiats.navigator.screen;

import java.io.Serializable;

/**
 * @author Andrei Zaiats
 * @since 09/30/2017
 */
public interface Screen extends Serializable {
}
