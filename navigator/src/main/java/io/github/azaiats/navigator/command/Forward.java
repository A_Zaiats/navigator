package io.github.azaiats.navigator.command;

import android.support.annotation.NonNull;

import io.github.azaiats.navigator.screen.Screen;

/**
 * @author Andrei Zaiats
 * @since 09/30/2017
 */
public class Forward implements Command {

    private final Screen screen;

    public Forward(@NonNull Screen screen) {
        this.screen = screen;
    }

    @NonNull
    public Screen getScreen() {
        return screen;
    }
}
