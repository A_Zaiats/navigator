package io.github.azaiats.navigator.result;

import android.support.annotation.Nullable;

/**
 * @author Andrei Zaiats
 * @since 09/30/2017
 */
public interface ResultListener {

    void onResult(@Nullable Object resultData);
}
