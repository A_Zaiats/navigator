package io.github.azaiats.navigator.command;

/**
 * @author Andrei Zaiats
 * @since 09/30/2017
 */
public interface Command {
    Back BACK = new Back();
}