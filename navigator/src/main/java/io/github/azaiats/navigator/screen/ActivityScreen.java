package io.github.azaiats.navigator.screen;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

/**
 * @author Andrei Zaiats
 * @since 09/30/2017
 */
public abstract class ActivityScreen implements Screen {

    @NonNull
    public abstract Intent getIntent(@NonNull Context context);
}
