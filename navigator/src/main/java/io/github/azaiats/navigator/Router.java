package io.github.azaiats.navigator;

import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.SparseArray;

import java.util.ArrayDeque;
import java.util.Queue;

import io.github.azaiats.navigator.command.Back;
import io.github.azaiats.navigator.command.BackTo;
import io.github.azaiats.navigator.command.Command;
import io.github.azaiats.navigator.command.Forward;
import io.github.azaiats.navigator.command.Replace;
import io.github.azaiats.navigator.command.SystemMessage;
import io.github.azaiats.navigator.result.ResultListener;
import io.github.azaiats.navigator.screen.Screen;

/**
 * @author Andrei Zaiats
 * @since 09/30/2017
 */
public class Router implements NavigatorHolder {

    private Navigator navigator;
    private Queue<Command> pendingCommands = new ArrayDeque<>();
    private SparseArray<ResultListener> resultListeners = new SparseArray<>();

    @Override
    @MainThread
    public void setNavigator(@NonNull Navigator navigator) {
        this.navigator = navigator;
        while (!pendingCommands.isEmpty()) {
            executeCommand(pendingCommands.poll());
        }
    }

    @Override
    @MainThread
    public void removeNavigator() {
        navigator = null;
    }

    /**
     * Open new screen and add it to the screens chain.
     */
    public void navigateTo(@NonNull Screen screen) {
        executeCommand(new Forward(screen));
    }

    /**
     * Clear the current screens chain and start new one
     * by opening a new screen right after the root.
     */
    public void newScreenChain(@NonNull Screen screen) {
        executeCommand(new BackTo(null));
        executeCommand(new Forward(screen));
    }

    /**
     * Clear all screens and open new one as root.
     */
    public void newRootScreen(@NonNull Screen screen) {
        executeCommand(new BackTo(null));
        executeCommand(new Replace(screen));
    }

    /**
     * Replace current screen.
     * By replacing the screen, you alters the backstack,
     * so by going back you will return to the previous screen
     * and not to the replaced one.
     */
    public void replaceScreen(@NonNull Screen screen) {
        executeCommand(new Replace(screen));
    }

    /**
     * Return back to the needed screen from the chain.
     * Behavior in the case when no needed screens found depends on
     * the processing of the {@link BackTo} command in a {@link Navigator} implementation.
     */
    public void backTo(@Nullable Class<Screen> screenClass) {
        executeCommand(new BackTo(screenClass));
    }

    /**
     * Return to the previous screen in the chain and send result data.
     */
    public void exitWithResult(int resultCode, @Nullable Object result) {
        exit();
        sendResult(resultCode, result);
    }

    /**
     * Return to the previous screen in the chain.
     * Behavior in the case when the current screen is the root depends on
     * the processing of the {@link Back} command in a {@link Navigator} implementation.
     */
    public void exit() {
        executeCommand(Command.BACK);
    }

    /**
     * Return to the previous screen in the chain and show system message.
     */
    public void exitWithMessage(@NonNull String message) {
        executeCommand(Command.BACK);
        executeCommand(new SystemMessage(message));
    }

    public void showSystemMessage(@NonNull String message) {
        executeCommand(new SystemMessage(message));
    }

    public void executeCommand(@NonNull Command command) {
        if (navigator != null) {
            navigator.applyCommand(command);
        } else {
            pendingCommands.offer(command);
        }
    }

    public void addResultListener(int resultCode, @NonNull ResultListener resultListener) {
        resultListeners.put(resultCode, resultListener);
    }

    public void removeResultListener(int resultCode) {
        resultListeners.remove(resultCode);
    }

    private boolean sendResult(int resultCode, Object result) {
        final ResultListener listener = resultListeners.get(resultCode);
        if (listener != null) {
            listener.onResult(result);
            return true;
        }
        return false;
    }
}
