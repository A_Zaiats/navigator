package io.github.azaiats.navigator.command;

import android.support.annotation.NonNull;

/**
 * @author Andrei Zaiats
 * @since 09/30/2017
 */
public class SystemMessage implements Command {

    private final String message;

    public SystemMessage(@NonNull String message) {
        this.message = message;
    }

    @NonNull
    public String getMessage() {
        return message;
    }
}
