package io.github.azaiats.navigator.screen;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

/**
 * @author Andrei Zaiats
 * @since 09/30/2017
 */
public abstract class SupportFragmentScreen implements Screen {

    @NonNull
    public abstract Fragment getFragment();
}
